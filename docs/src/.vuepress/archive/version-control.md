---
title: 'Version Control'
---

### How Meltano Does Version Control

> **Note**: This section is a WIP, and not currently functioning as written. This note will be removed once it does work as advertised.  
> Items marked with `**` are not yet implemented.

Meltano runs many different types of files and projects including but not limited to:

1. Extractors
1. Loaders
1. dbt Transforms
1. Meltano Models
1. Jupyter Notebook files \*\*
1. Airflow DAGs as part of an orchestration step \*\*
